<!DOCTYPE html>
<html>
<head>
	<title>Hospital</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
.dropbtn {
    background-color: white;
    color: black;
    
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
</style>
</head>
<body>
<div>
	<div class="row" style="margin-top: 2%;margin-left: 3%">
	  <div class="col-sm-2">
	 	<img src="images/logo.png">
	  </div>
	  <div class="col-sm-1 dropbtn" style="background-color: white;margin-top: 2%"><a href="home.php"><font color="black">Home</font></a></div>
	  <div class="dropdown col-sm-1" style="margin-top: 2%">
        <button class="dropbtn">Doctors</button>
        <div class="dropdown-content">
          <a href="cardic.php">Cardiac Care</a>
          <a href="#">Boan & Joint Care</a>
          <a href="#">Neuro Care</a>
          <a href="#">Hard Care</a>
          <a href="#">Liver Care</a>
        </div>
      </div>
      <div class="dropdown col-sm-2" style="margin-top: 2%">
        <button class="dropbtn">Centres & Excellence</button>
        <div class="dropdown-content">
          <a href="#">Cardiac Care</a>
          <a href="#">Boan & Joint Care</a>
          <a href="#">Neuro Care</a>
          <a href="#">Hard Care</a>
          <a href="#">Liver Care</a>
          <a href="#">Child Care</a>
          <a href="#">Woman Care</a>
        </div>
      </div>
	    <div class="col-sm-1 dropbtn" style="background-color: white; margin-top: 2%"><a href="about.php"><font color="black">About Us</font></a></div>
	    <div class="col-sm-2 dropbtn" style="background-color: white; margin-top: 2%"><a href="contact.php"><font color="black">Contact Us</font></a></div>
	    <div class="col-sm-1" style=" margin-top: 2%;margin-left: 15%"><img src="images/facebooklogo.png" height="30px;"><img src="images/facebooklogo.png"
	     height="30px;"></div>
    </div>
</div>

</body>
</html>