<html>
<title>Hospital</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>

<div class="w3-content w3-section" style="max-width:80%; max-height: 80%">
  <img class="mySlides w3-animate-fading" src="images/first.jpg " style="width:100%;height: 100%">
  <img class="mySlides w3-animate-fading" src="images/slider6.jpg" style="width:100%;height: 100%"">
  <img class="mySlides w3-animate-fading" src="images/slider7.jpg" style="width:100%;height: 100%"">
  <img class="mySlides w3-animate-fading" src="images/slider8.jpg" style="width:100%;height: 100%"">
</div>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 9000);    
}
</script>

</body>
</html>
