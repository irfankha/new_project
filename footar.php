<!DOCTYPE html>
<html>
<head>
	<title>Hospital</title>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="row jumbotron" style="margin-left: 10%; margin-right: 10%">
	<div class="col-sm-4">
		<font color="green"><h3>About the hospital</h3></font>
		We are Fortis. For over 26 years, we have been committed to the cause of getting people back to their lives faster and stronger. With 56 hospitals across the nation and over 10,000 beds, Fortis Healthcare Limited is a leading integrated healthcare delivery service provider in India.
	</div>
	<div class="col-sm-4">
		<font color="green"><h3>Our Speciality Areas</h3></font>
		<div style="margin-top: 6%">Cardiology and Cardiac Surgery</div>
		<div>Cardiac Care</div>
		<div>Orthopaedic</div>
		<div>Neurology and Neurosurgery</div>
		<div>Women & Child Care</div>
		<div>Hard Care</div>
		<div>Liver Care</div>
	</div>
	<div class="col-sm-4">
		<font color="green"><h3>Helpline Numbers</h3></font>
		<div style="margin-top: 6%">Bannerghatta Road: 105711<br>
            Cunningham Road: 105711<br>
            Rajajinagar: 080 2300 4444<br>
            Nagarbhavi: 080 2301 4122 
        </div>   
	</div>
</div>
<div style="background-color: lightblue; width: 100%; height: 50px; margin-top: 20px"><font size="5px" style="margin-left: 10%">Hospital Footar</font></div>
</body>
</html>