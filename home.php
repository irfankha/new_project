<!DOCTYPE html>
<html>
<head>
	<title>Hospital</title>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	 <style type="text/css">
	 	.resultitem:hover{
          background:#59f;
  -       webkit-transform: scale(1.1);
          transform: scale(1.1);
}
	 </style>
</head>
<body>
<div>
<?php
include 'header.php';
?>

</div>
<div>
	<?php include 'slider.php' ?>
</div>
<div class="row" style="margin-left: 10%; margin-right: 10%">
	<div class="col-sm-4 jumbotron resultitem" style="background-color: white">
	    <center><img src="images/centers.png"></center>
		<center><h3>Centres of Excellence</h3></center>
		Fortis Hospitals Bangalore’s clinical and surgical centres of excellence with great pre and post care facilities ensure that your loved ones get back to the best of their health in no time.
		<center><a href="#"><img src="images/button.JFIF" width="80px" style="margin-top: 5%"></a></center>
	</div>
	<div class="col-sm-4 jumbotron resultitem">
	    <center><img src="images/doctor.png"></center>
		<center><h3>Find a Doctor</h3></center>
		If you are searching for trusted, eminent and experienced doctors you can end your search here. Find the leading doctors, surgeons and clinical specialists at Fortis Bangalore.
		<center><a href="#"><img src="images/button.JFIF" width="80px" style="margin-top: 5%"></a></center>
	</div>
	<div class="col-sm-4 jumbotron resultitem" style="background-color: lightgreen">
	    <center><img src="images/appointment.png"></center>
		<center><h3>Book an Appointment </h3></center>
		Prevention is always better than cure and your trusted health care specialist is just a click away. We suggest you book an appointment at your preferred location, date and doctor.
		<center><a href="#"><img src="images/button.JFIF" width="80px" style="margin-top: 5%"></a></center>
	</div>
</div>
<div style="margin-left: 10%; margin-right: 10%">
      <center><div ><button type="" class="btn btn-success">All DEPARTMENTS</button></div></center>
	<div class="row" style="margin-top: 1%">
		<div class="col-sm-3 jumbotron" style="background-color: white">
		    <a href="#">
		         <center><img src="images/Best-In-class-Services.png"></center>
		    </a>
			<b><h4>Best-Class-Service</h4></b>
			Fortis Hospitals is one of the leading healthcare delivery providers in India. It is ranked No.2 Globally in the List of Technologically most advanced hospitals in the world.
		</div>
		<div class="col-sm-3 jumbotron">
		     <a href="#">
		         <center><img src="images/Qualified-Doctors.png"></center>
		     </a>
			 <b><h4>Qualified Doctors</h4></b>
			 Fortis Hospitals in Bangalore has some of the best doctors, surgeons and visiting consultants in the country across all its speciality areas.
		</div>
		<div class="col-sm-3 jumbotron" style="background-color: lightblue">
		    <a href="#">
                 <center><img src="images/24-Hours-Service.png"></center>
            </a>
			<b><h4>24 Hours Service</h4></b>
			We are available 24 Hours at your service to bring to you are the best healthcare services to help you or your loved one get fit in no time.
		</div>
		<div class="col-sm-3 jumbotron" style="background-color: lightgreen">
		    <a href="">
		           <center><img src="images/International-Patient-Services.png"></center>
		    </a>
			<b><h4>International Patient Services</h4></b>
			Our advanced healthcare technology combined with years of expertise has helped us become one of the world’s top destinations for medical tourism.
		</div>
	</div>
</div>
<div>
	<?php include 'footar.php' ?>
</div>
</body>
</html>